<?php
require '../../vendor/autoload.php';
config('source', '../app.ini');

ORM::configure('sqlite:../../resources/db.sqlite');

// get route for index
get('/', function () {
    $flash = flash('status');
    $item_to_buy = ORM::for_table('items')
        ->select('items.*')
        ->where_equal('items.bought', 0)
        ->order_by_asc('name')
        ->find_many();

    $item_bought = ORM::for_table('items')
        ->select('items.*')
        ->where_equal('items.bought', 1)
        ->find_many();

    render(
        'main', 
        array(
            'item_to_buy' => $item_to_buy,
            'item_bought' => $item_bought,
            'flash' => $flash
        )
    );
});

post('/edit/:id', function($id) {
    $item = stash('item');
    if ($item) {
        $item->name = from($_POST, 'item');
        $item->save();
        flash('status','Désignation modifée : '.$item->name);
    } else {
        flash('status', 'Erreur de saisie');
    }
    header('Location: /manage');
});

get('/edit/:id', function($id) {
    $item = stash('item');
    if ($item) {
        render('add',
            array('item' => $item)
        );
    }else {
        flash('status', 'Erreur de saisie');
        header('Location: /manage');
    }
});

get('/add', function() {
    $flash = flash('status');
    render('add',
        array('flash' => $flash)
    );
});

get('/manage', function() {
    $flash = flash('status');

    $item_to_buy = ORM::for_table('items')
        ->select('items.*')
        ->where_equal('items.bought', 0)
        ->order_by_asc('name')
        ->find_many();

    $item_bought = ORM::for_table('items')
        ->select('items.*')
        ->where_equal('items.bought', 1)
        ->find_many();        
    render('manage',
            array(
                'item_to_buy' => $item_to_buy,
                'item_bought' => $item_bought,
                'flash' => $flash
        )
    );
});


// get route for index
post('/add', function () {
    $item = ORM::for_table('items')->create();
    $item->name = from($_POST, 'item');
    $item->save();
    flash('status', 'Elément '.$item->name.' a été ajouté');
    header('Location: /add');
});

// get route for index
get('/done/:id', function ($id) {
    $item = stash('item');
    if ($item) {
        $item->bought = 1;
        $item->save();
        flash('status','Elément '.$item->name.' marqué comme acheté');
    } else {
        flash('status', 'Erreur de saisie');
    }
    header('Location: /');    
});

// get route for index
get('/undone/:id', function ($id) {
    $item = stash('item');
    if ($item) {
        $item->bought = 0;
        $item->save();
        flash('status','Elément '.$item->name.' marqué comme à acheter');
    } else {
        flash('status', 'Erreur de saisie');
    }
    header('Location: /');    
});

// preload blog entry whenever a matching route has :blog_id in it
filter('id', function ($id) {

    $item = ORM::for_table('items')
        ->where_equal('id', $id)
        ->find_one();
    // stash() lets you store stuff for later use (NOT a cache)
    stash('item', $item);
});


// Serve the blog
dispatch();
?>