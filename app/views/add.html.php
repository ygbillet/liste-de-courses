<?php 
if (isset($item)) {
    $value = 'value="'.$item->name.'"';
    $action = "/edit/".$item->id;
} else {
    $value = '';
    $action = '/add';
}

?>

<form method="POST" action="<?php echo $action ?>">
    <div class="row">
        <div class="large-6 columns">
            <div class="row collapse">
                <div class="small-10 columns">
                    <input type="text" name="item" placeholder="Nom" <?php echo $value ?>>
                </div>
                <div class="small-2 columns">
                    <input type="submit" class="button prefix" />
                </div>
            </div>
        </div>    
    </div>
</form>