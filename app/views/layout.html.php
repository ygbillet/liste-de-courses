<!DOCTYPE html>
<!--[if IE 8]>               <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<html>
<head>

<head>
    <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <title>Liste de courses - v0.1</title>

  <link rel="stylesheet" href="/css/foundation.min.css" />
  <script src="/js/vendor/custom.modernizr.js"></script>

</head>
<body>
<nav class="top-bar">
  <ul class="title-area">
    <!-- Title Area -->
    <li class="name">
      <h1><a href="#">Courses</a></h1>
    </li>
    <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
  </ul>

  <section class="top-bar-section">
    <!-- Left Nav Section -->
    <ul class="right">
      <li class="divider"></li>
      <li><a href="/">Liste</a></li>
      <li class="divider"></li>
      <li><a href="/add">Ajout</a></li>
      <li class="divider"></li>
      <li><a href="/manage">Changer</a></li>
      <li class="divider"></li>
    </ul>
  </section>
</nav>

    <div class="row">
        <div class="large-12 columns">
            <?php if(isset($flash)):?>
            <div data-alert class="alert-box secondary radius">
                <p><?php echo $flash ?></p>
                <a href="#" class="close">&times;</a>
            </div>
            <?php endif; ?>
            <section id="content">

                <?php echo content()?>

            </section>
        </div>
    </div>
    <script>
    document.write('<script src=' +
    ('__proto__' in {} ? 'js/vendor/zepto' : 'js/vendor/jquery') +
    '.js><\/script>')
    </script>

    <script src="/js/foundation.min.js"></script>

    <script src="/js/foundation/foundation.js"></script>

    <script src="/js/foundation/foundation.dropdown.js"></script>

    <script src="/js/foundation/foundation.placeholder.js"></script>

    <script src="/js/foundation/foundation.alerts.js"></script>

    <script src="/js/foundation/foundation.topbar.js"></script>
    <script>
        $(document).foundation();
    </script>    

</body>
</html>