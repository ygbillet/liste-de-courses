
<h2>A acheter</h2>
<ul class="no-bullet">
<?php foreach($item_to_buy as $item):?>
        <li><a class="button radius expand" href="/done/<?php echo($item->id) ?>"><?php echo($item->name); ?></a></li>
<?php endforeach;?>
</ul>

<h2>Acheté</h2>
<ul class="no-bullet">
<?php foreach($item_bought as $item):?>
    <li><a class="button split radius expand secondary" href="/undone/<?php echo($item->id) ?>"><?php echo($item->name); ?></a></li>
<?php endforeach;?>
</ul>